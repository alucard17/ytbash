#!/bin/bash
#youtube bash app created by alucard17 (alucard17@gitlab) using yt-dlp
#this code is licensed under the GNU public license GPL v.3

figlet ytbash
echo What do you want to do?
options=(Search WatchID URL Quit)
select menu in "${options[@]}";

do
    if [[ $menu == "Search" ]]; then
        echo Enter Querry:
        read querry
        echo How many results?
        read results
        yt-dlp -e --get-id --get-duration "ytsearch$results:$querry"
    elif [[ $menu == "WatchID" ]]; then
        echo Enter watch-id:
        read watchid
        yt-dlp -o - "https://www.youtube.com/watch?v=$watchid" | mpv -
    elif [[ $menu == "URL" ]]; then
        echo Enter video-url:
        read vidurl
        yt-dlp -o - "$vidurl" | mpv -
    else
        break;
    fi
done
