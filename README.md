# ytbash

## update
Since there has been quite some problems with youtube-dl lately, I decided to transission from youtube-dl to yt-dlp. Not only does it fix a lot of shortcomings, but all in all it seems to have great functionality. Fore more info see [yt-dlp](https://github.com/yt-dlp/yt-dlp).

For this reason, the new software requirements will be: yt-dlp, mpv and figlet.

As for the old version, figlet is optional and the specific part may be removed from the code if not needed. Similarly mpv may be switched for some similar software.

As with the old version, the file needs to be made executable with `chmod +x youtube.sh`, and then it can be executed via `./youtube.sh`.

For simplicity you may want to create an alias.


## old version
A simple youtube bash app to pipe youtube videos directly to a specified player, in this case mpv, using youtube-dl.

Software needed: figlet, youtube-dl and mpv.

If you do not want to use figlet just comment it out in the code, and feel free to pipe to your favourite media player instead of using mpv.

The file needs to be made executable with `chmod +x youtube_old.sh`, and then it can be executed via `./youtube_old.sh`.

For simplicity you may want to create an alias.
